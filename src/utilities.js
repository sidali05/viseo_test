const getOwner = async username => {
  const api = `https://api.github.com/users/${username}`;
  return fetch(api).then(async res => {
    return await res.json();
  });
};

const getRepo = async username => {
  const api = `https://api.github.com/users/${username}/repos`;
  return fetch(api).then(async res => {
    return await res.json();
  });
};

let common = {
  getOwner,
  getRepo,
};
export default common;
