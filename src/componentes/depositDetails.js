import React from 'react';
import {StyleSheet, View, Text, SafeAreaView} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import AppHeader from './Header';
import {Avatar} from 'react-native-elements';

class DepositDetails extends React.Component {
  render() {
    const {repo} = this.props.navigation.state.params;
    const {owner} = repo;
    return (
      <>
        <SafeAreaView style={styles.container}>
          <AppHeader />

          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <View style={styles.avatar}>
                <Avatar
                  rounded
                  source={{
                    uri: owner.avatar_url,
                  }}
                />
              </View>
              <Text style={styles.login}>{owner.login}</Text>
              <Text style={styles.url}>{owner.url}</Text>
            </View>
            <View style={styles.projectContainer}>
              <Text style={styles.projectName}>{repo.name}</Text>
              <Text style={styles.projectTechno}>{repo.language}</Text>
              <Text style={styles.projectDesc}>{repo.description}</Text>
            </View>
          </View>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    marginTop: 8,
  },
  avatar: {
    marginTop: -22,
    padding: -81,
  },
  container: {
    borderWidth: 1,
    borderColor: 'grey',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  projectContainer: {
    marginTop: 62,
    paddingHorizontal: 24,
    textAlign: 'center',
  },

  url: {
    marginTop: 2,
    paddingHorizontal: 37,
    color: 'grey',
  },

  sectionTitle: {
    fontSize: 24,
    fontWeight: '100',
    color: Colors.black,
  },
  login: {
    marginTop: -35,
    fontSize: 15,
    fontWeight: '400',
    color: Colors.dark,
    textAlign: 'center',
  },
  projectName: {
    fontSize: 25,
    fontWeight: '400',
    color: Colors.dark,
    textAlign: 'center',
  },
  projectTechno: {
    fontSize: 15,
    fontWeight: '400',
    color: Colors.dark,
    textAlign: 'center',
  },
  projectDesc: {
    marginTop: 30,
    fontSize: 15,
    fontWeight: '400',
    color: Colors.dark,
    textAlign: 'center',
  },
});

export default DepositDetails;
