import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  View,
  Alert,
} from 'react-native';
import pages from '../navigation/page';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import AppHeader from './Header';
import {SearchBar, Button} from 'react-native-elements';
import common from '../utilities';

class SearchScreen extends React.Component {
  state = {
    search: null,
    repos: null,
    owner: null,
  };

  updateSearch = search => {
    this.setState({search});
  };
  handelOnPress = async () => {
    const {search} = this.state;
    if (search) {
      const reposResult = await common.getRepo(search);
      const ownerRes = await common.getOwner(search);
      reposResult && this.setState({repos: reposResult});
      ownerRes && this.setState({owner: ownerRes});
    } else {
      Alert.alert('Error', 'please enter usename');
    }
  };

  render() {
    const {search, repos, owner} = this.state;
    if (repos !== null && owner != null) {
      this.props.navigation.navigate(pages.DEPOSITLIST, {
        repos: repos,
        owner: owner,
      });
    }
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <AppHeader />
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.searchContainer}>
              <SearchBar
                placeholder="Username..."
                onChangeText={this.updateSearch}
                value={search}
                platform="ios"
              />
            </View>
            <View style={styles.searchButtonContainer}>
              <Button
                title="Search"
                buttonStyle={styles.searchButton}
                onPress={() => {
                  this.handelOnPress();
                }}
              />
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.white,
  },
  searchContainer: {
    marginTop: 200,
    marginLeft: 20,
    marginRight: 100,
  },
  searchButton: {
    backgroundColor: '#ff6781',
  },
  searchButtonContainer: {
    marginLeft: 280,
    marginRight: 20,
    marginTop: -60,
  },
});

export default SearchScreen;
