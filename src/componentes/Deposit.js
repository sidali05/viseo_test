import React from 'react';
import {StyleSheet, View, Text, TouchableWithoutFeedback} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import pages from '../navigation/page';
const Deposit = ({repo, navigation}) => {
  return (
    <>
      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate(pages.DEPOSITDETAILS, {repo: repo});
        }}>
        <View style={styles.body}>
          <View style={styles.sectionContainer}>
            <Icon name="computer" size={15} color="#000" />
            <Text style={styles.sectionDescription}>{repo.name}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

const styles = StyleSheet.create({
  body: {
    marginTop: 8,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '100',
    color: Colors.black,
  },
  sectionDescription: {
    marginLeft: 120,
    marginTop: -14,
    fontSize: 15,
    fontWeight: '400',
    color: Colors.dark,
    alignItems: 'center',
  },
});

export default Deposit;
