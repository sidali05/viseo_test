import React from 'react';
import {Header} from 'react-native-elements';

const AppHeader = () => {
  return (
    <Header
      placement="left"
      leftComponent={{icon: 'menu', color: '#fff', marginTop: -10}}
      centerComponent={{
        text: 'Github Dashboard Sample',
        style: {color: '#fff', fontSize: 24, marginTop: -10},
      }}
    />
  );
};

export default AppHeader;
