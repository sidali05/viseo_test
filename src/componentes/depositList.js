import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  StatusBar,
  ScrollView,
  View,
} from 'react-native';
import AppHeader from './Header';
import Deposit from './Deposit';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {ListItem} from 'react-native-elements';

class DepositList extends React.Component {
  render() {
    const {repos, owner} = this.props.navigation.state.params;
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.container}>
          <AppHeader />
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.avatar}>
              <ListItem
                leftAvatar={{
                  title: owner.login,
                  source: {uri: owner.avatar_url},
                  showEditButton: true,
                }}
                title={owner.login}
              />
            </View>
            {repos &&
              repos.map(r => (
                <Deposit
                  key={r.id}
                  repo={r}
                  navigation={this.props.navigation}
                />
              ))}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    marginTop: 8,
  },
  avatar: {
    marginTop: 6,
    marginLeft: 96,
    marginRight: 96,
    backgroundColor: '#C0C0C0',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '100',
    color: Colors.black,
  },
  sectionDescription: {
    marginLeft: 120,
    marginTop: -14,
    fontSize: 15,
    fontWeight: '400',
    color: Colors.dark,
    alignItems: 'center',
  },
  container: {
    flex: 1,
  },
  login: {
    marginTop: -30,
    fontSize: 15,
    marginLeft: 82,
    textAlign: 'center',
  },
});

export default DepositList;
