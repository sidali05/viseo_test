import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import pages from './page';
import Home from '../componentes/SearchScreen';
import DepositList from '../componentes/DepositList';
import DepositDetails from '../componentes/DepositDetails';
const AppNavigator = createStackNavigator(
  {
    [pages.HOME]: Home,
    [pages.DEPOSITLIST]: DepositList,
    [pages.DEPOSITDETAILS]: DepositDetails,
  },
  {
    initialRouteName: pages.HOME,
    mode: 'modal',
    headerMode: 'none',
    defaultNavigationOptions: {
      gestureEnabled: false,
    },
  },
);
const Navigation = createAppContainer(AppNavigator);

export default Navigation;
